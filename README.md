# acetylene-network

Interface for making network requests and handling data. "API layer" (sorta).

## roadmap

- [ ] Finish alpha schema
  - [ ] substance
  - [ ] reaction
  - [ ] procedure
- [ ] build appropriate RESTy functions
- [ ] add sugar